# **Proyecto NetBeans** #

El juego del **'Trivial'** para el idioma *Español*.

![Captura2.PNG](https://bitbucket.org/repo/7EEKEny/images/162088440-Captura2.PNG)

# **Metodología:** #

1. No Swing

1. MVC

1. Constantes

1. Expresiones regulares

1. Ficheros de texto

1. Ficheros de acceso aleatorio