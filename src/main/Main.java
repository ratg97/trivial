package main;

import controller.ListController;
import controller.GameController;
import controller.SettingsController;
import java.io.IOException;
import java.util.ArrayList;
import model.Question;
import utils.Constants;
import utils.GameUtils;
import utils.MenuUtils;
import utils.RandomFileUtils;
import utils.TextFileUtils;
import view.Display;

public class Main {

    public static void main(String[] args) throws IOException {

        if (!Constants.QUESTIONS_RANDOM_FILE.exists() || !Constants.ANSWERS_RANDOM_FILE.exists() || !(Constants.QUESTIONS_RANDOM_FILE.length() > 0) || !(Constants.ANSWERS_RANDOM_FILE.length() > 0)) {
            ArrayList<Question> questions = new ArrayList<Question>();
            TextFileUtils.readTextFile(questions);
            RandomFileUtils.createFilesFirstTime(questions);
            //Borramos el ArrayList ya que no es necesario
            questions.clear();
        }
        int operationMenu;
        do {
            operationMenu = MenuUtils.getOperation();
            switch (operationMenu) {
                case Constants.PLAY_VALUE:
                    boolean repeat;
                    do {
                        ListController list = new ListController();
                        list.setQuestionsGame();
                        SettingsController settings = new SettingsController(list);
                        settings.setSettingsRight();
                        GameController game = new GameController(settings);
                        game.play();
                        repeat = GameUtils.repeat();
                    } while (repeat);
                    Display.writeln("\n---------");
                    break;
                case Constants.INSTRUCTIONS_VALUE:
                    Display.writeln("\nEste trivial consiste en que el jugador tendrá que:\nen una pregunta escoger entre 3 respuestas, la correcta.\nSaldrán 10 preguntas siempre a menos que no hubiera tantas.\n");
                    Display.writeln("---------");
                    break;
            }
        } while (operationMenu != Constants.EXIT_VALUE);

    }
}