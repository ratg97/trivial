package utils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import view.Display;

public class GameUtils {

    private GameUtils() {
    }

    public static int askAnswer() {
        int number = Constants.LOOP_VALUE;
        boolean rigthAnswer;
        do {
            try {
                do {
                    Display.write("--> Introduce tu respuesta " + Constants.REGEX_ANSWER_GAME + ": ");
                    number = new Scanner(System.in).nextInt();
                    if (!isAnswer(number)) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (!isAnswer(number));
                rigthAnswer = true;
            } catch (final Exception e) {
                rigthAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rigthAnswer);
        return number;
    }

    private static boolean isAnswer(int answer) {
        Pattern pat = Pattern.compile(Constants.REGEX_ANSWER_GAME);
        Matcher mat = pat.matcher("" + answer);
        return mat.matches();
    }

    public static boolean repeat() {
        boolean rightAnswer;
        int repeatOperation = Constants.LOOP_VALUE;
        Display.writeln("---------");
        Display.writeln("¿Otra partida?");
        Display.writeln(" " + Constants.REPEAT_VALUE + ":" + Constants.REPEAT_NAME);
        Display.writeln(" " + Constants.NO_REPEAT_VALUE + ":" + Constants.NO_REPEAT_NAME);
        Display.writeln("---------\n");
        do {
            try {
                do {
                    Display.write("--> Introduce tu opción: ");
                    repeatOperation = new Scanner(System.in).nextInt();
                    if (repeatOperation != Constants.REPEAT_VALUE && repeatOperation != Constants.NO_REPEAT_VALUE) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (repeatOperation != Constants.REPEAT_VALUE && repeatOperation != Constants.NO_REPEAT_VALUE);
                rightAnswer = true;
            } catch (final Exception e) {
                rightAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rightAnswer);
        return repeatOperation == Constants.REPEAT_VALUE;
    }
}
