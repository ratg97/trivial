package utils;

import java.util.Scanner;
import view.Display;

public class MenuUtils {

    private MenuUtils() {
    }

    public static int getOperation() {
        int number = Constants.LOOP_VALUE;
        boolean rigthAnswer;
        Display.writeln(Constants.GAME_NAME);
        Display.writeln(" " + Constants.PLAY_VALUE + ":" + Constants.PLAY_NAME);
        Display.writeln(" " + Constants.INSTRUCTIONS_VALUE + ":" + Constants.INSTRUCTIONS_NAME);
        Display.writeln(" " + Constants.EXIT_VALUE + ":" + Constants.EXIT_NAME);
        Display.writeln("---------\n");
        do {
            try {
                do {
                    Display.write("--> Introduce tu opción: ");
                    number = new Scanner(System.in).nextInt();
                    if (number != Constants.PLAY_VALUE
                            && number != Constants.INSTRUCTIONS_VALUE
                            && number != Constants.EXIT_VALUE) {
                        Display.writeln("\nIntroduce un valor correcto\n");
                    }
                } while (number != Constants.PLAY_VALUE
                        && number != Constants.INSTRUCTIONS_VALUE
                        && number != Constants.EXIT_VALUE);
                rigthAnswer = true;
            } catch (final Exception e) {
                rigthAnswer = false;
                Display.writeln("\nIntroduce un valor correcto\n");
            }
        } while (!rigthAnswer);
        return number;
    }
}
