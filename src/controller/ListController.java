package controller;

import java.io.IOException;
import static java.lang.System.exit;
import java.util.ArrayList;
import model.Question;
import utils.Constants;
import utils.RandomFileUtils;

public class ListController {

    private ArrayList<Question> questionsFromRandomFile;

    public ListController() {
        this.questionsFromRandomFile = new ArrayList<Question>();
    }

    public void setQuestionsGame() throws IOException {
        
        if (RandomFileUtils.isNQuestionsEqualsNAnswers()) {
            //Si hay más de 10 pues se escogen 10, sino las que haya
            while (this.questionsFromRandomFile.size() != (RandomFileUtils.getNQuestions() >= Constants.NQUESTION_GAME ? Constants.NQUESTION_GAME : RandomFileUtils.getNQuestions())) {
                Question q = RandomFileUtils.readQuestionRandomFile();
                if (!checkQuestionRepeat(this.questionsFromRandomFile, q)) {
                    this.questionsFromRandomFile.add(q);
                }
            }
        }
    }

    private boolean checkQuestionRepeat(ArrayList<Question> questionsFromRandomFile, Question q) {
        for (Question que : questionsFromRandomFile) {
            if (que.getQuestion().equalsIgnoreCase(q.getQuestion())) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Question> getQuestionsFromRandomFile() {
        return questionsFromRandomFile;
    }
}
