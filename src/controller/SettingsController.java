package controller;

public class SettingsController {

    private boolean settingsRight;
    private ListController list;

    public SettingsController(ListController list) {
        this.list = list;
    }

    public boolean isSettingsRight() {
        return settingsRight;
    }

    public void setSettingsRight() {
        this.settingsRight = !this.list.getQuestionsFromRandomFile().isEmpty();
    }

    public ListController getList() {
        return list;
    }
}
