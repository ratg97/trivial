package controller;

import static java.lang.System.exit;
import utils.GameUtils;
import view.Display;

public class GameController {

    private SettingsController settings;

    public GameController(
            SettingsController settings) {
        this.settings = settings;
    }

    public void play() {
        if (this.settings.isSettingsRight()) {
            int nQuestions = this.settings.getList().getQuestionsFromRandomFile().size();
            boolean finish = false;
            int i = 0;
            int correctInt = 0;
            while (i < nQuestions && !finish) {
                Display.writeln("\nPreguntas restantes: " + (nQuestions - i) + "/" + nQuestions + " | Puntos: " + correctInt);
                showQuestion(i);
                int answerInt = GameUtils.askAnswer();
                String answerString = this.settings.getList().getQuestionsFromRandomFile().get(i).getAnswers()[answerInt - 1];
                if (checkAnswer(answerString, i)) {
                    Display.writeln("\n+1");
                    correctInt++;
                } else {
                    Display.writeln("\n-1");
                    correctInt--;
                    Display.writeln("\nLa correcta era: " + this.settings.getList().getQuestionsFromRandomFile().get(i).getSolution() + "\n");
                }
                i++;
                if (!(i < nQuestions)) {
                    finish = true;
                }
            }
            Display.writeln("\nPuntos: " + correctInt);
        } else {
            Display.writeln("--> Hay problemas con la lista cargada...\n");
            exit(0);
        }
    }

    private void showQuestion(int i) {

        Display.writeln("\n" + this.settings.getList().getQuestionsFromRandomFile().get(i).getQuestion());
        int x = 0;
        for (String qS : this.settings.getList().getQuestionsFromRandomFile().get(i).getAnswers()) {
            x++;
            Display.writeln("\t" + x + ") " + qS);
        }

    }

    private boolean checkAnswer(String answerString, int i) {
        return (this.settings.getList().getQuestionsFromRandomFile().get(i).getSolution().equalsIgnoreCase(answerString));
    }
}
